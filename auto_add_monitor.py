#coding=utf-8 
#!/bin/bash
#读取process.txt和process_jobcenter.txt

import MySQLdb
import re
import os
import sys

# 打开数据库连接
db = MySQLdb.connect("192.168.88.63", "root", "c4xRVCY2uECX1XAqcJyQy", "ioms", charset='utf8' )
# 使用cursor()方法获取操作游标 
cursor = db.cursor()

for line in open("process.txt"): 
    host = line.split(",")[0]
    alias = line.split(",")[1]
    pname = alias.split("/")[-1]

    former_sql = "select id from process_process_daemon where buildenv = '%s' and alias = '%s'" % (host, alias)
    latter_sql = "insert into process_process_daemon(pname, alias, buildenv, port, pstatus, retry_time, user, standard_num) values('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');" % (pname, alias, host, "32022", "1", "0", "webadmin", "1")
    result = cursor.execute(former_sql)
    if result:
        #已存在记录，则不进行插入
        print("已存在 %s-%s 记录，不进行插入" % (host, alias))
    else:
        #不存在记录则插入
        print("正在插入 %s-%s " % (host, alias))
        cursor.execute(latter_sql)
        db.commit()

for line in open("process_jobcenter.txt"):
    host = line.split(",")[0]
    alias = line.split(",")[1]
    pname = alias.split("/")[-1]

    former_sql = "select id from process_process_daemon where buildenv = '%s' and alias = '%s'" % (host, alias)
    latter_sql = "insert into process_process_daemon(pname, alias, buildenv, port, pstatus, retry_time, user, standard_num) values('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');" % (pname, alias, host, "32022", "1", "0", "webadmin", "1")
    result = cursor.execute(former_sql)
    if result:
        #已存在记录，则不进行插入
        print("已存在 %s-%s 记录，不进行插入" % (host, alias))
    else:
        #不存在记录则插入
        print("正在插入 %s-%s " % (host, alias))
        cursor.execute(latter_sql)
        db.commit()

cursor.close()
db.close()

     
