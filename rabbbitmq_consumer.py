# !/usr/bin/env python
import pika
credentials = pika.PlainCredentials('wusheng','wusheng')
connection = pika.BlockingConnection(pika.ConnectionParameters(
    '192.168.10.50',5672,'/',credentials))
channel = connection.channel()

# 声明queue
channel.queue_declare(queue='balance')

def callback(ch, method, properties, body):
   print("[x] Received %r" % body)

channel.basic_consume('balance', callback, auto_ack=True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()


