#!/usr/bin/python3

import jenkins
import json

counts = ""

class Jenkins(object):
    def __init__(self, url, username, password): 
        self.url = url
        self.username = username 
        self.password = password

    #验证jenkins登录是否成功
    #def jenkins_login_verify(self):
    #    try:
    #        server = jenkins.Jenkins(url=self.url, username=self.username, password=self.password)
    #        version = server.get_version()
    #    except Exception as e:
    #        code = "500"
    #        return json.dumps({"code": code, "result": "%s"%(e)})
    #    else:
    #        code = "200"
    #        return json.dumps({"code": code})

    #jenkins登录
    def jenkins_login(self):
        try:
            server = jenkins.Jenkins(url=self.url, username=self.username, password=self.password)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return server

    def jenkins_get_version(self):
        try:
            version = self.jenkins_login().get_version()
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return version            

    #返回jenkins任务个数
    def jenkins_jobs_count(self):
        try:
            counts = self.jenkins_login().jobs_count()
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return counts

    #返回jenkins某个job的信息
    def jenkins_get_job_info(self, name, depth=0, fetch_all_builds=False):
        try:
            job_info = self.jenkins_login().get_job_info(name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return job_info

    #根据输入的参数返回项目名称，若无该项目则返回None
    def jenkins_get_job_name(self, name):
        try:
            job_name = self.jenkins_login().get_job_name(name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return job_name

    def jenkins_job_open(self, req, add_crumb=True, resolve_auth=True):
        try:
            job_name = self.jenkins_login().jenkins_open(req)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return job_name 

    #获取jenkins某一次构建信息
    def jenkins_get_build_info(self, name, number, depth=0):
        try:
            jenkins_build_info = self.jenkins_login().get_build_info(name, number, depth=0)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_build_info

    #获取jenkins某一次构建环境变量
    def jenkins_get_build_env_vars(self, name, number, depth=0):
        try:
            jenkins_build_env_vars = self.jenkins_login().get_build_env_vars(name, number, depth=0)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_build_env_vars 

    #获取所有job信息，name,url,color,fullname
    def jenkins_get_all_jobs(self, folder_depth=None):
        try:
            jenkins_all_jobs = self.jenkins_login().get_all_jobs(folder_depth=None)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_all_jobs

    def jenkins_get_jobs(self, folder_depth=0, view_name=None):
        try:
            jenkins_jobs = self.jenkins_login().get_jobs()
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_jobs

    #jenkins复制jobs
    def jenkins_copy_job(self, from_name, to_name):
        try:
            jenkins_copy_job = self.jenkins_login().copy_job(from_name, to_name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_copy_job
  
    def jenkins_delete_job(self, name):
        try:
            jenkins_delete_job = self.jenkins_login().delete_job(name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_delete_job  

    #jenkins重命名jobs
    def jenkins_rename_job(self, from_name, to_name):
        try:
            jenkins_rename_job = self.jenkins_login().rename_job(from_name, to_name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_rename_job

    #jenkins启用job
    def jenkins_enable_job(self, name):
        try:
            jenkins_enable_job = self.jenkins_login().enable_job(name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_enable_job

    #jenkins禁用job
    def jenkins_disable_job(self, name):
        try:
            jenkins_enable_job = self.jenkins_login().disable_job(name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_enable_job

    #判断job是否存在，存在返回True
    def jenkins_job_exists(self, name):
        try:
            jenkins_job_exists = self.jenkins_login().job_exists(name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_job_exists

    #如果job不存在，返回不存在信息
    def jenkins_assert_job_exists(self, name, exception_message='job[%s] does not exist'):
        try:
            jenkins_assert_job_exists = self.jenkins_login().assert_job_exists(name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_assert_job_exists

    #创建任务
    def jenkins_create_job(self, name, config_xml):
        try:
            jenkins_create_job = self.jenkins_login().create_job(name, config_xml)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_create_job
   
    #获取任务的config配置
    def jenkins_get_job_config(self, name):
        try:
            jenkins_get_job_config = self.jenkins_login().get_job_config(name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_get_job_config

    #重新配置job
    def jenkins_reconfig_job(self, name, config_xml):
        try:
            jenkins_reconfig_job = self.jenkins_login().reconfig_job(name, config_xml)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_reconfig_job 

    #返回job构建链接
    def jenkins_build_job_url(self, name, parameters=None, token=None):
        try:
            jenkins_build_job_url = self.jenkins_login().build_job_url(name, parameters=None, token=None)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_build_job_url
  
    #构建job         
    def jenkins_build_job(self, name, parameters=None, token=None):
        try:
            jenkins_build_job = self.jenkins_login().build_job(name, parameters=None, token=None)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_build_job    

    #停止构建
    def jenkins_stop_build(self, name, number):
        try:
            jenkins_stop_build = self.jenkins_login().stop_build(name, number)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_stop_build

    #删除构建
    def jenkins_delete_build(self, name, number):
        try:
            jenkins_delete_build = self.jenkins_login().delete_build(name, number)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_delete_build

    #获取正在构建的任务列表
    def jenkins_get_running_builds(self):
        try:
            jenkins_get_running_builds = self.jenkins_login().get_running_builds()
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_get_running_builds

    #获取node数量
    def jenkins_get_nodes(self, depth=0):
        try:
            jenkins_get_nodes = self.jenkins_login().get_nodes()
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_get_nodes 

    #获取node信息
    def jenkins_get_node_info(self, name, depth=0):
        try:
            jenkins_get_node_info = self.jenkins_login().get_node_info(name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_get_node_info

    #删除node节点
    def jenkins_delete_node(self, name):
        try:
            jenkins_delete_node = self.jenkins_login().delete_node(name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_delete_node

    #禁用node节点
    def jenkins_disable_node(self, name, msg=""):
        try:
            jenkins_disable_node = self.jenkins_login().disable_node(name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_disable_node

    def jenkins_enable_node(self, name):
        try:
            jenkins_enable_node = self.jenkins_login().enable_node(name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_enable_node

    def jenkins_node_exists(self, name):
        try:
            jenkins_node_exists = self.jenkins_login().node_exists(name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_node_exists
 
    def jenkins_assert_node_exists(self, name, exception_message='node[%s] does not exist'):
        try:
            jenkins_assert_node_exists = self.jenkins_login().assert_node_exists(name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_assert_node_exists

    def jenkins_get_node_config(self, name):
        try:
            jenkins_get_node_config = self.jenkins_login().get_node_config(name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_get_node_config

    def jenkins_reconfig_node(self, name, config_xml):
        try:
            jenkins_reconfig_node = self.jenkins_login().reconfig_node(name, config_xml)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_reconfig_node

    #获取构建日志
    def jenkins_get_build_console_output(self, name, number):
        try:
            jenkins_get_build_console_output = self.jenkins_login().get_build_console_output(name, number)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_get_build_console_output

    #获取视图名
    def jenkins_get_view_name(self, name):
        try:
            jenkins_get_view_name = self.jenkins_login().get_view_name(name)
        except Exception as e:
            code = "500"
            return json.dumps({"code": code, "result": "%s"%(e)})
        else:
            return jenkins_get_view_name

url = "http://xxx/"
username = "xxx"
password = "xxx"

jen_init = Jenkins(url=url, username=username, password=password)
print(jen_init.jenkins_get_job_config("test1"))
