# -*- coding:utf-8 -*-

import flask, json
from flask import request
import re, requests

webhook = "https://oapi.dingtalk.com/robot/send?access_token=bd38f7e0be7a15dd0db6d8d89a9bff3d722ad96c763a7492ce5a5c0822729744"
mobile = "13588287451" 

# 创建一个服务，把当前这个python文件当做一个服务
server = flask.Flask(__name__)
@server.route('/callback', methods=['get', 'post'])
def callback():
    code = "200"
    if request.method == 'POST':
        params = request.get_json()
        print(params)
        #判断类型
        if re.findall(str(params['label']), "sexy"):
            params['label'] = "性感"
        elif re.findall(str(params['label']), "porn"):
            params['label'] = "涉黄"
        elif re.findall(str(params['label']), "normal"):
            params['label'] = "正常"
        else:
            params['label'] = "未知"
    
        #判断是否需要复审
        if re.findall(str(params['review']), "false"):
            params['review'] = "不需要"
        else:
            params['review'] = "需要" 
 
        #替换字典key
        params.update(存储空间 = params.pop("hub"), 完整流名 = params.pop("streamId"), 房间号 = params.pop("stream"), 准确率 = params.pop("rate"), 人工复审 = params.pop("review"), 时间 = params.pop("time"),类型 = params.pop("label"), 文件 = params.pop("ts"),)

        result = ""
        for key, value in params.items():
            result += str(key) +": "+ str(value) + "\n"
        Robot_Send(result)
    return json.dumps({"code": code}, ensure_ascii=False)

def Robot_Send(message):
    url = webhook
    headers = {
        "Content-Type": "application/json ;charset=utf-8 "
    }
    data = {
        "msgtype": "text",
        "text": {"content": message},
        "at": {
            "atMobiles": [ mobile ],
            "isAtAll": False
        }
    }
    format_data = json.dumps(data, sort_keys=True,indent = 4) #转为json
    res = requests.post(url, data=format_data, headers=headers)
    return res

if __name__ == '__main__':
    server.run(debug=True, port=8888, host='0.0.0.0')





