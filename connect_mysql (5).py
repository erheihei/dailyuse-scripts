#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import MySQLdb
import re
import os
import commands

# 打开数据库连接
db = MySQLdb.connect("10.10.10.34", "root", "c4xRVCY2uECX1XAqcJyQy", "ioms", charset='utf8' )
# 使用cursor()方法获取操作游标 
cursor = db.cursor()

server_deploy_ip = "10.10.10.42"
#jobcenter 没有启动端口， chamet-web 没有启动端口
sql = '''select * from publish_publish where server_deploy_env = "线上环境" and target like "%10.10.10.42%" and server_name like "%java%"'''
try:
    #清空记录文件
    f=open('/etc/zabbix/scripts/process.txt', "w+")
    f.seek(0)
    f.truncate()

    f=open('/etc/zabbix/scripts/process_jobcenter.txt', "w+")
    f.seek(0)
    f.truncate()

    # 执行SQL语句
    cursor.execute(sql)
    # 获取所有记录列表
    results = cursor.fetchall()
    for result in results:
        server_name = result[1]
        server_type = result[3]
        
        #print(server_name)
        #print(server_type) 
        if server_type == "dubbo1":
            #dubbo监控端口或者日志项目
            server_ports = result[7].split(",")
            pname = result[10] + "/" + result[11]
            if re.findall("report-jobcenter", server_name):
                pid = os.popen("pgrep -f %s"%(pname))
                ppid = pid.readlines()
                if re.search("null", pname):
                    continue
                if len(ppid) <= 0:
                    ppid = "-1"
                else:
                    ppid = str(ppid[0])
                with open('/etc/zabbix/scripts/process_jobcenter.txt','a+') as f:
                    f.write(server_deploy_ip+","+pname+","+ppid.strip()+"\n")
            elif re.findall("jobcenter", server_name):
                plogpath = result[14] + 'newcomlog'
                listdirs = os.listdir(plogpath)
                for listdir in listdirs:
                    pfile = plogpath+"/"+listdir
                    if re.search("null", pfile):
                        continue
                    pid = os.popen("pgrep -f %s"%(pfile))
                    ppid = pid.readlines()
                    if len(ppid) <= 0:
                        ppid = "-1"
                    else:
                        ppid = str(ppid[0])
                    with open('/etc/zabbix/scripts/process_jobcenter.txt','a+') as f:
                        f.write(server_deploy_ip+","+pfile+","+ppid.strip()+"\n")
            else:
                for server_port in server_ports:
                    with open('/etc/zabbix/scripts/process.txt','a+') as f:
                        f.write(server_deploy_ip+","+pname+","+server_port+"\n")
        elif server_type == "dubbo2":
            #dubbo前端项目
            pass
        elif server_type == "backend":
            #dubbo tomcat类型项目
            pname = result[10]
            server_ports = result[7].split(",")
            for server_port in server_ports:
                with open('/etc/zabbix/scripts/process.txt','a+') as f:
                    f.write(server_deploy_ip+","+pname+","+server_port+"\n")
    db.close()
except Exception as e:
    print(e)
