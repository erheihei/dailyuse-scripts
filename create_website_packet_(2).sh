#!/bin/bash
#0-青柠交友 1-青柠直播 2-草莓约普通版 3-草莓约华为版 4-富聊1对1

url=$1

if [ "${url##*.}" == "apk" ];then
	internal_version=$2
        version=$3
        typeid=$4
        md5=$5        
elif [ "${url##*.}" == "ipa" ];then
        version=$2
        old_version=$3
        typeid=$4
        md5=$5
else
        echo "./$0 url internal_version version typeid md5 or ./$0 url version old_version typeid md5" && exit 1
fi

#ios
function ios_package(){
	dir="" #下载目录
	logfile=""
	result=""

	if [ "$typeid" == "0" ];then
		#青柠交友
		dir="/datapackages/download/ipa/qn_friend"
		logfile="${dir}/ios_package.log"  
		ios_package_old="${dir}/qnfriend_${old_version}"
		ios_package_new="${dir}/qnfriend_${version}"
		ios_package_name="${dir}/qnfriend.ipa"
		download_url="http://ipa.96friend.cn/qn_friend/qnfriend.ipa"
	elif [ "$typeid" == "1" ];then
		#青柠直播
		dir="/datapackages/download/ipa/qingning"
        	logfile="${dir}/ios_package.log"
		ios_package_old="${dir}/qnchat_${old_version}"
        	ios_package_new="${dir}/qnchat_${version}"
        	ios_package_name="${dir}/qnchat.ipa"
		download_url="http://ipa.96friend.cn/qingning/qnchat.ipa"
	elif [ "$typeid" == "2" ];then
		#草莓约普通版
		dir="/datapackages/download/ipa/berries"
		logfile="${dir}/ios_package.log"
		ios_package_old="${dir}/berries${old_version}"
                ios_package_new="${dir}/berries${version}"
                ios_package_name="${dir}/berries.ipa"
		download_url=""
		echo "敬请期待" && exit 1
	elif [ "${typeid}" == "3" ];then
                #草莓约华为版
                dir="/datapackages/download/ipa/berries"
                logfile="${dir}/ios_package.log"
		ios_package_old="${dir}/berries_huawei_${old_version}"
                ios_package_new="${dir}/berries_huawei_${version}"
                ios_package_name="${dir}/berries_huawei.ipa"
		download_url=""
		echo "敬请期待" && exit 1
        elif [ "${typeid}" == "4" ];then
                #富聊1对1
                dir="/datapackages/download/ipa/fuliao1v1"
                logfile="${dir}/ios_package.log"
		ios_package_old="${dir}/fuliao1v1_${old_version}"
                ios_package_new="${dir}/fuliao1v1_${version}"
                ios_package_name="${dir}/fuliao1v1.ipa"	
		download_url=""
        elif [ "${typeid}" == "5" ];then
		#蜜约
                dir="/datapackages/download/ipa/miyue"
                logfile="${dir}/ios_package.log"
                ios_package_old="${dir}/miyue_${old_version}"
                ios_package_new="${dir}/miyue_${version}"
                ios_package_name="${dir}/miyue.ipa"
		download_url=""
                echo "敬请期待" && exit 1
        fi

	if [ ! -f ${ios_package_old}.plist ] || [ ! -f ${ios_package_old}.html ];then
        	echo -e "\n`date`: ${ios_package_old} html或plist文件不存在，打包失败" >> $logfile 2>&1 && exit 1 
    	fi
	
	echo -e "\n`date`: ios打包开始，开始下载${ios_package_new}.ipa" >> $logfile 2>&1
	if [ -f ${ios_package_name} ];then
		#备份
		cp ${ios_package_name} ${ios_package_name}.bak
	fi
    	wget -O ${ios_package_new}.ipa $url >> /dev/null 2>&1
	#延迟确保下载完成，md5正常
        sleep 10
	echo -e "\n`date`: ${ios_package_new}.ipa 下载结束" >> $logfile 2>&1
	md5_ipa=`md5sum ${ios_package_new}.ipa | awk -F ' ' '{print $1}'`
	echo -e "\nmd5: $md5_ipa \ninput_md5: $md5\nurl: $url \nversion: $version\nold_version: $old_version" >> $logfile 2>&1
    	if [ "$md5_ipa" == "$md5" ];then
        	cp ${ios_package_new}.ipa ${ios_package_name}
		#替换plist
		cp ${ios_package_old}.plist ${ios_package_new}.plist			
		cp ${ios_package_old}.html ${ios_package_new}.html

		sed -i 's/'''${old_version}'''/'''${version}'''/g' ${ios_package_new}.plist
		sed -i 's/'''${old_version}'''/'''${version}'''/g' ${ios_package_new}.html
		
		chmod 666 ${ios_package_old}.plist ${ios_package_old}.html
		chmod 666 ${ios_package_new}.plist ${ios_package_new}.html
		
       		echo "`date`: md5值一致,打包成功" >> $logfile 2>&1
		#flushcdn $download_url
		result=0
    	else
        	echo "`date`: md5值不一致,打包失败" >> $logfile 2>&1
		result=1
    	fi
	echo $result
}

#andriod
function android_package(){
	dir="" #下载目录
	logfile=""
	result=""

	if [ "$typeid" == "0" ];then
		#青柠交友
		dir="/datapackages/download/apk/qn_friend"
		logfile="${dir}/android_package.log"
        	android_package_new="${dir}/qnfriend_${version}"
        	android_package_name="${dir}/qnfriend.apk"
		download_url="http://apk.96friend.cn/qn_friend/qnfriend.apk"
	elif [ "${typeid}" == "1" ];then
		#青柠直播
		dir="/datapackages/download/apk/qingning"
		logfile="${dir}/android_package.log"
		android_package_new="${dir}/qnchat_${version}"
        	android_package_name="${dir}/qnchat.apk"
		download_url="http://apk.96friend.cn/qingning/qnchat.apk"
	elif [ "${typeid}" == "2" ];then
		#草莓约普通版
                dir="/datapackages/download/apk/berries"
                logfile="${dir}/android_package.log"
                android_package_new="${dir}/berries${version}"
                android_package_name="${dir}/berries.apk"
		download_url="http://apk.xingketech.com/berries/berries.apk"
	elif [ "${typeid}" == "3" ];then
		#草莓约华为版
		dir="/datapackages/download/apk/berries"
                logfile="${dir}/android_package.log"
                android_package_new="${dir}/berries_huawei_${version}"
                android_package_name="${dir}/berries_huawei.apk"
		download_url="http://apk.xingketech.com/berries/berries_huawei.apk"
	elif [ "${typeid}" == "4" ];then
		#富聊1对1
                dir="/datapackages/download/apk/fuliao1v1"
                logfile="${dir}/android_package.log"
                android_package_new="${dir}/fuliao1v1_${version}"
                android_package_name="${dir}/fuliao1v1.apk"
		download_url="http://apk.96friend.cn/fuliao1v1/fuliao1v1.apk"
        elif [ "${typeid}" == "5" ];then
                #蜜约
                dir="/datapackages/download/apk/miyue"
                logfile="${dir}/android_package.log"
                android_package_new="${dir}/miyue_${version}"
                android_package_name="${dir}/miyue.apk"
                download_url="http://apk.cokar.cn/fuliao1v1/fuliao1v1.apk"
        elif [ "${typeid}" == "6" ];then
                #蜜岛
                dir="/midao_download/apk/midao"
                logfile="${dir}/android_package.log"
                android_package_new="${dir}/midao_${version}"
                android_package_name="${dir}/midao_${internal_version}.apk"
                download_url="http://apk.iwjyx.com/midao/midao_${internal_version}.apk"
		index_dir="/aliyun_ossfs/www/webH5/product/pcWebsite/views/midao"
		sed -i "s/midao.apk/midao_${internal_version}.apk/g" ${index_dir}/../../js/midao/index_m.js
		sed -i "s/midao.apk/midao_${internal_version}.apk/g" ${index_dir}/index.html
                sed -i "s/midao.apk/midao_${internal_version}.apk/g" ${index_dir}/index1.html
		chmod 777 ${index_dir}/../../js/midao/index_m.js ${index_dir}/index.html ${index_dir}/index1.html
        elif [ "${typeid}" == "7" ];then
		#伊对缘
		dir="/datapackages/download/apk/yiduiyuan"
                logfile="${dir}/android_package.log"
                android_package_new="${dir}/yiduiyuan_${version}"
                android_package_name="${dir}/yiduiyuan.apk"
                download_url="http://apk.kukerr.com/yiduiyuan/yiduiyuan.apk"
	elif [ "${typeid}" == "8" ];then
                #番茄约
                dir="/datapackages/download/apk/fanqieyue"
                logfile="${dir}/android_package.log"
                android_package_new="${dir}/fanqieyue_${version}"
                android_package_name="${dir}/fanqieyue.apk"
                download_url="http://apk.curker.com/fanqieyue/fanqieyue.apk"
	elif [ "${typeid}" == "9" ];then
                #g聊
                dir="/datapackages/download/apk/gchat"
                logfile="${dir}/android_package.log"
                android_package_new="${dir}/gchat_${version}"
                android_package_name="${dir}/gchat.apk"
                download_url="http://apk.xingcan8.com/gchat/gchat.apk"
	fi

	echo -e "\n`date`: 安卓打包开始，开始下载${android_package_new}.apk" >> $logfile 2>&1
	if [ -f ${android_package_name} ];then
		#备份
		cp ${android_package_name} ${android_package_name}.bak
	fi
    	wget -O ${android_package_new}.apk $url >> /dev/null 2>&1
	#延迟确保下载完成，md5正常
	sleep 10
	echo -e "\n`date`: ${android_package_new}.apk 下载结束" >> $logfile 2>&1 
    	md5_apk=`md5sum ${android_package_new}.apk | awk -F ' ' '{print $1}'`
    	echo -e "\nmd5: ${md5_apk} \ninput_md5: ${md5} \nurl: ${url} \nversion: ${version}" >> $logfile 2>&1
    	if [ "$md5_apk" == "$md5" ];then
        	cp ${android_package_new}.apk ${android_package_name}
        	echo "`date`: md5值一致,打包成功" >> $logfile 2>&1
		#flushcdn $download_url
		result=0
    	else
		cp ${android_package_name}.bak ${android_package_name}
        	echo "`date`: md5值不一致,打包失败" >> $logfile 2>&1
		result=1
    	fi             
	echo $result         
}
#android_package

#cdn,刷新目录
function flushcdn(){
	for((i=1;i<=2;i++));  
	do
		#刷新文件
		result=$(python /data/scripts/guanwang/flushcdn.py  RefreshObjectCaches ${download_url} File)
		echo "第$i次刷新文件："$result >> $logfile 2>&1
        	download_url=${download_url%/*}"/"
		#刷新目录
		result=$(python /data/scripts/guanwang/flushcdn.py  RefreshObjectCaches ${download_url} Directory)		
		echo "第$i次刷新目录："$result >> $logfile 2>&1
	done
}

if [ "${url##*.}" == "apk" ];then
    	android_package
elif [ "${url##*.}" == "ipa" ];then
	ios_package
else
	echo "./$0 url version typeid md5 or ./$0 url version old_version typeid md5" && exit 1
fi


