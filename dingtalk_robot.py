#!/usr/bin/python
import sys
import io
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
import time
import requests
import json
import urllib

mobile = sys.argv[1]
message = sys.argv[2]
webhook = sys.argv[3]

def Robot_Send(message):
    url = webhook 
    headers = {
        "Content-Type": "application/json ;charset=utf-8 "
    }
    data = {
        "msgtype": "text",
        "text": {"content": message},
        "at": {
            "atMobiles": [ mobile ],
            "isAtAll": False
        }
    }
    format_data = json.dumps(data)
    res = requests.post(url, data=format_data, headers=headers)
    return res.text

if __name__ == '__main__':
    Robot_Send(message)
