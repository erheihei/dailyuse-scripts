import paramiko
import configparser

class ParamikoClient:
    def __init__(self, file):
        self.client = paramiko.SSHClient()
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.config = configparser.ConfigParser()
        self.config.read(file)
 
    def connect(self):
        try:
            self.client.connect(
                hostname = self.config.get('ssh','host'), 
                port = self.config.getint('ssh','port'), 
                username = self.config.get('ssh','username'), 
                password = self.config.get('ssh','password'), 
                timeout = self.config.getfloat('ssh','timeout')
            )
        except Exception as e:
            print(e)
            try:
                self.client.close()
            except:
                pass
 
    def runcmd(self, cmd):
        stdin, stdout, stderr = self.client.exec_command(cmd)
        return stdout.read().decode()

from ParamikoClient import ParamikoClient
 
def process():
    client = ParamikoClient('config.conf')
    client.connect()
    client.runcmd('date')
 
if __name__ == '__main__':
    process()
