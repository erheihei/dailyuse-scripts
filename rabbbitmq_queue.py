import os, sys, time
import requests
import json

class RabbitMQTool(object):
    def __init__(self, host, user, passwd):
        self.host = host
        self.user = user
        self.passwd = passwd
    # 返回3种消息数量：ready, unacked, total
    def getMessageCount(self):
        url = 'http://%s:15672/api/queues' % (self.host)
        r = requests.get(url, auth=(self.user, self.passwd))
        if r.status_code != 200:
            return -1
        return r.text 
if __name__ == '__main__':
    mqTool = RabbitMQTool(host='192.168.88.60',
                          user='wusheng',
                          passwd='wusheng')
    dic = json.loads(mqTool.getMessageCount())
    for i in range(len(dic)):
        print("队列:" + dic[i]['name'] + ", vhost:" + dic[i]['vhost'] + ", 节点:" + dic[i]['node'] + ", 消费者:"+ str(dic[i]['consumers']) + ", 持久化:" + str(dic[i]['durable']) + ", 消耗内存:" + str(dic[i]['memory']) + ", 队列长度:" + str(dic[i]['backing_queue_status']['len']))
