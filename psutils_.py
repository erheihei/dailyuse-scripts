#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import psutil  
import datetime  
import sys
import os

#第一个参数 进程参数名
pname = sys.argv[1]

pid = os.popen("pgrep -f %s"%(pname))
pid = pid.readlines()
print(pid)
p = psutil.Process(int(pid[0].strip())) 
pstatus = p.status()
pcreate_time = p.create_time()
pcpu_time = p.cpu_times()
pexe = p.exe()
pcwd = p.cwd()
puids = p.uids()
pgids = p.gids()

print("pname: "+pname + "\n" + "pstatus: " + str(pstatus) + "\n" + "pcreate_time: " + str(pcreate_time) + "\n" + "pcpu_time: " + str(pcpu_time) + "\n" + "exe: " + str(pexe) + "\n" + "pcwd: " + str(pcwd) + "\n" + "puids:" + str(puids))
