#!/bin/bash
#下载python3环境

python3_dir="/usr/local/python3"
download_dir=`pwd`
#安装依赖
yum -y groupinstall "Development tools"
yum -y install zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel wget

if [ ! -f $python3_dir ];then
        echo "`date`: 新建python3目录: $python3_dir"
        mkdir $python3_dir
fi

#下载python3安装包
cd $download_dir
wget https://www.python.org/ftp/python/3.6.2/Python-3.6.2.tar.xz
tar -xvJf Python-3.6.2.tar.xz && cd Python-3.6.2 && ./configure --prefix="$python3_dir"
make && make install

#建立软连接
ln -s /usr/local/python3/bin/python3 /usr/bin/python3
ln -s /usr/local/python3/bin/pip3 /usr/bin/pip3

#测试
python3 -V
