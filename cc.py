#!/usr/bin/python3

# -*- coding: UTF-8 -*-
from urllib import request
if __name__ == "__main__":
    #以CSDN为例，CSDN不更改User Agent是无法访问的
    url = 'http://www.ipaychat.net/'
    head = {}
    #写入User Agent信息
    head['User-Agent'] = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.168 Safari/537.36'
    req = request.Request(url, headers=head)  #创建Request对象
    response = request.urlopen(req) #传入创建好的Request对象
    html = response.read().decode('utf-8')  #读取响应信息并解码
    print(html) #打印信息
