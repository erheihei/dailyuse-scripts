#!/bin/bash
#author wusheng
#revoke openvpn

path="/etc/openvpn" #/etc/openvpn
easyRsa_path="easy-rsa/3"
client_id=$1
client_database_path="${path}/easy-rsa/3.0.3/pki"

function usage(){
    echo "please enter client_id" && exit 1
}

#判断参数个数
if [ $# != 1 ];then
	usage
fi

function revoke_openvpn(){
	cd ${path}/$easyRsa_path
	#执行撤销指令
	expect<<EOF
	spawn ./easyrsa revoke ${client_id}
	expect {
    		"*revocation:*" {
      		send "yes\r"
      		exp_continue
    	}
    	"*ca.key:*" {
      		send "root\r"
      		exp_continue
    	}
}	
EOF

expect<<EOF
	spawn ./easyrsa gen-crl
	expect {
    		"*ca.key:*" {
      		send "root\r"
      		exp_continue
    	}
}
EOF

cd pki/ && chmod 666 crl.pem

#删除配置
rm -rf ${path}/ccd/${client_id} ${client_database_path}/private/${client_id}.key ${client_database_path}/reqs/${client_id}.req ${client_database_path}/issued/${client_id}.crt ${path}/client/${client_id}.zip
}
revoke_openvpn
