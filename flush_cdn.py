#!/usr/bin/python
#coding=utf-8

from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.acs_exception.exceptions import ClientException
from aliyunsdkcore.acs_exception.exceptions import ServerException
from aliyunsdkcdn.request.v20180510.RefreshObjectCachesRequest import RefreshObjectCachesRequest
from aliyunsdkcdn.request.v20180510.DescribeRefreshQuotaRequest import DescribeRefreshQuotaRequest
import re

#domain_list = ['96friend.cn', 'shelinkme.cn', 'xingketech.com', 'cokar.cn', 'video2friend.com', 'kukerr.com', 'iwjyx.com', 'midaochat.net', '5151jiaoyou.com', 'bingo28.cn', 'gamepk8.cn', 'curker.com', 'tooya8.com', 'cqtykjwl.com']
#富聊
#shelinkme.cn 96friend,cn
#旺角
#midaochat.net 5151jiaoyou.com iwjyx.com bingo28.cn gamepk8.cn
#酷科
#kukerr.com video2friend.com cokar.cn
#兔牙
#curker.com tooya8.com cqtykjwl.com
#星客
#xingketech.com

class flushcdn(object):
    '''
    cdn刷新类
    '''
    def __init__(self, action, path, type):
        self.action = action
        self.path = path
        self.type = type

    def get_company(self):
        '''
        根据域名，返回scret和key
        :return:
        '''
        if re.search("96friend.cn", self.path) or re.search("shelinkme.cn", self.path):
            # 富聊
            accessKeyId = 'LTAIfoKOgr112xq1'
            accessSecret = '31dkXntOKQL4Qac16rtVQ2x3dOQj6E'
        elif re.search("midaochat.net", self.path) or re.search("5151jiaoyou.com", self.path) or re.search(
                "iwjyx.com", self.path) or re.search("bingo28.cn", self.path) or re.search("gamepk8.cn", self.path):
            # 旺角
            accessKeyId = 'LTAIMXh8a4mQLwPL'
            accessSecret = 'mJ8w4XF7tDI5K4BSLcSCbmm3If3Yx3'
        elif re.search("kukerr.com", self.path) or re.search("video2friend.com", self.path) or re.search("cokar.cn",
                                                                                                           self.path):
            # 酷科
            accessKeyId = 'LTAIci88j95XT6Rd'
            accessSecret = '8HyIzn3ZgRl7z5Dz2o6pPjCIyhMw9l'
        elif re.search("curker.com", self.path) or re.search("tooya8.com", self.path) or re.search("cqtykjwl.com",
                                                                                                     self.path):
            # 兔牙
            accessKeyId = 'LTAI0jqpYtf2DKvh'
            accessSecret = 'J7qsZuJBiT1iOOXEA6PArJHH0Kq3QZ'
        elif re.search("xingketech.com", self.path):
            # 星客
            accessKeyId = 'LTAIfKxhmM7csFTq'
            accessSecret = '76SMqM92Ap3jCzXfykP2cNOGap0F1I'
        elif re.search("ichamet.com", self.path) or re.search("schamet.com", self.path) or re.search("uchamet.com", self.path):
            # 聊遇
            accessKeyId = 'LTAI4FpM7cceXb4H1AgEyoVK'
            accessSecret = 'WVD8PF0nYThzh1nd4mIIq2BnR1SGri'

        return accessKeyId, accessSecret

    def flush_cdn(self):
        '''
        刷新cdn
        :return:
        '''
        accessKeyId, accessSecret = self.get_company()
        client = AcsClient(accessKeyId, accessSecret, 'cn-hangzhou')
        request = RefreshObjectCachesRequest()
        request.set_accept_format('json')
        request.set_ObjectType(self.type)
        request.set_ObjectPath(self.path)
        response = client.do_action_with_exception(request)
        return response

    def flush_times(self):
        '''
        获取剩余次数
        :return:
        '''
        accessKeyId, accessSecret = self.get_company()
        client = AcsClient(accessKeyId, accessSecret, 'cn-hangzhou')
        request = DescribeRefreshQuotaRequest()
        request.set_accept_format('json')
        response = client.do_action_with_exception(request)
        return str(response, 'utf-8')

#hello = flushcdn("RefreshObjectCaches", "http://apk.96friend.cn/qnfriend/", "Directory")
#print(hello.flush_cdn())
#print(hello.flush_times())
