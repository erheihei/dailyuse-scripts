#!/usr/bin/python3
import paramiko

#实例化一个对象
s = paramiko.SSHClient()

#自动添加策略,首次登陆用交互式确定
s.set_missing_host_key_policy(paramiko.AutoAddPolicy())

#建立私钥文件连接
key = paramiko.RSAKey.from_private_key_file('/root/.ssh/id_rsa')

#通过私钥验证的方式登录远程主机
s.connect(hostname='127.0.0.1', port=22, username='root', pkey=key)

#要执行的命令
stdin, stdout, stderr = s.exec_command('ls /root')

#查看命令的执行结果
print(stdout.read())
