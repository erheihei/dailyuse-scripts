#!/bin/bash

vpn_dir="/etc/openvpn/config/"
for file in `ls $vpn_dir | grep -v openvpnip_manifest`
do
	cat $vpn_dir/$file | while read line
	do
		vpn_name=`echo $line | awk -F ' ' '{print $1}'`
		vpn_ip=`echo $line | awk -F ' ' '{print $2}'`
		echo ${vpn_name}","${vpn_ip}
	done
done
