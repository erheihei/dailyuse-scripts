#-*- coding: utf-8 -*-
#!/usr/bin/python3

import json
import requests

class disconf_api(object):
    '''
    disconf 操作
    '''
    def __init__(self, url, username, password, remember=0):
        self.url = url
        self.username = username
        self.password = password
        self.remember = remember

    def disconf_login(self):
        '''
        disconf 登录
        '''
        login_url = self.url + "/api/account/signin"
        body = {'name': self.username, 'password': self.password, 'remember': self.remember}
  
        res = requests.post(login_url, data=body)
        return res

    def disconf_logout(self):
        '''
        disconf 退出
        '''
        login = self.disconf_login()
        logout_url = self.url + "/api/account/signout"
        
        res = requests.get(logout_url, cookies=login.cookies)
        return res.text

    def disconf_session(self):
        '''
        获取disconf session
        '''
        login = self.disconf_login()
        session_url = self.url + "/api/account/session"
        res = requests.get(session_url, cookies=login.cookies)
        return res.text
  
    def disconf_env_list(self):
        '''
        返回所有环境的列表
        '''    
        login = self.disconf_login()
        env_list_url = self.url + "/api/env/list"
        res = requests.get(env_list_url, cookies=login.cookies)
        return res.text

    def disconf_create_config(self):
        '''
        创建配置项
        '''
        config_url = self.url + "/api/web/config/item"
        return True

    def disconf_version_list(self, appid, envid=""):
        '''
        根据app, env 获取所有的 版本列表
        '''
        version_url = self.url + "/api/web/config/versionlist"
        params = {'appId': appid, 'envId': envid}
        
        login = self.disconf_login()
        res = requests.get(version_url, params=params, cookies=login.cookies)
        return res.text

    def disconf_config_list(self, appid, envid, version):
        '''
        根据app, env , version 获取所有的 配置列表，含有machine size, machine list,error num 等信息
        '''
        config_url = self.url + "/api/web/config/list"
        params = {'appId': appid, 'envId': envid, 'version': version}
     
        login = self.disconf_login()
        res = requests.get(config_url, params=params, cookies=login.cookies)
        return res.text

    def disconf_config_simple_list(self, appid, envid, version):
        '''
        据app, env , version 获取所有的 配置列表，无machine size, machine list,error num 等信息
        '''
        config_url = self.url + "/api/web/config/simple/list"
        params = {'appId': appid, 'envId': envid, 'version': version}

        login = self.disconf_login()
        res = requests.get(config_url, params=params, cookies=login.cookies)
        return res.text

    def disconf_get_config_content(self, configid):
        '''
        获取某个config的内容
        '''
        config_url = self.url + "/api/web/config/" + configid

        login = self.disconf_login()
        res = requests.get(config_url, cookies=login.cookies)
        return res.text

    def disconf_download_config(self, configid):
        '''
        以下载文件的形式下载配置
        '''
        config_url = self.url + "/api/web/config/download/" + configid

        login = self.disconf_login()
        res = requests.get(config_url, cookies=login.cookies)
        return res.text

    def disconf_download_configs(self, appid, envid, version):
        '''
        以下载文件的形式批量下载配置
        '''
        config_url = self.url + "/api/web/config/downloadfilebatch"
        params = {'appId': appid, 'envId': envid, 'version': version}

        login = self.disconf_login()
        res = requests.get(config_url, params=params, cookies=login.cookies)
        return res.text

    def disconf_update_config(self, configid, value):
        '''
        修改配置项的值
        '''
        config_url = self.url + "/api/web/config/item/" + configid
        params = {'configId': configid, "value": value}
 
        login = self.disconf_login()
        res = requests.put(config_url, params=params, cookies=login.cookies)
        return res.text

    def disconf_app_list(self):
        '''
        返回所有app的列表
        '''
        app_url = self.url + "/api/app/list"

        login = self.disconf_login()
        res = requests.get(app_url, cookies=login.cookies)
        return res.text 

#dis_api = disconf_api("http://test181-config.ipaychat.net", "gusixue", "PU09luy0bGeNLOpv")
#print(dis_api.disconf_version_list("4", "2"))
#print(dis_api.disconf_app_list())

