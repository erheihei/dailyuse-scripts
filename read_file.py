def read_file(filename):
    global location
    location = 0
    with open(filename) as fd:
        while True:
            cur_location = fd.tell()#记录文件当前位置
            if cur_location == location: #如果两者相等，说明没有新增文件
                continue
            else:
                data = fd.seek(location).read()#读取增量内容
                location = cur_location#移动指针到当前位置

content = read_file("/root/nohup.out")
print(content)
