import logging
import sys
import paramiko
from paramiko import AuthenticationException
from paramiko.client import SSHClient, AutoAddPolicy
from paramiko.ssh_exception import NoValidConnectionsError
 
 
class MySshClient():
    def __init__(self):
        self.ssh_client = SSHClient()
 
    # 此函数用于输入用户名密码登录主机
    def ssh_login_password(self, host_ip, username, password):
        try:
            # 设置允许连接known_hosts文件中的主机（默认连接不在known_hosts文件中的主机会拒绝连接抛出SSHException）
            self.ssh_client.set_missing_host_key_policy(AutoAddPolicy())
            #密码验证
            self.ssh_client.connect(host_ip,port=22,username=username,password=password)
            #建立私钥文件连接
            key = paramiko.RSAKey.from_private_key_file('/root/.ssh/id_rsa')
            #秘钥验证
            self.ssh_client.connect(host_ip, port=22, username=username, pkey=key) 
        except AuthenticationException:
            logging.warning('username or password error')
            return 1001
        except NoValidConnectionsError:
            logging.warning('connect time out')
            return 1002
        except:
            logging.warning('unknow error')
            print("Unexpected error:", sys.exc_info()[0])
            return 1003
        return 1000
 
    def ssh_login_rsa(self,host_ip, username, rsafile):
        try:
            # 设置允许连接known_hosts文件中的主机（默认连接不在known_hosts文件中的主机会拒绝连接抛出SSHException）
            self.ssh_client.set_missing_host_key_policy(AutoAddPolicy())
            #建立私钥文件连接
            key = paramiko.RSAKey.from_private_key_file(rsafile)
            #秘钥验证
            self.ssh_client.connect(host_ip, port=22, username=username, pkey=key)
        except AuthenticationException:
            logging.warning('username or password error')
            return 1001
        except NoValidConnectionsError:
            logging.warning('connect time out')
            return 1002
        except:
            logging.warning('unknow error')
            print("Unexpected error:", sys.exc_info()[0])
            return 1003
        return 1000
 
    # 此函数用于执行command参数中的命令并打印命令执行结果
    def execute_some_command(self,command):
        stdin, stdout, stderr = self.ssh_client.exec_command(command)
        print(stdout.read().decode())
 
    # 此函数用于退出登录
    def ssh_logout(self):
        logging.warning('will exit host')
        self.ssh_client.close()
 
if __name__ == '__main__':
    # 远程主机IP
    host_ip = '127.0.0.1'
    # 远程主机用户名
    username = 'root'
    # 远程主机密码
    #password = 'root'
    rsafile = '/root/.ssh/id_rsa'
    # 要执行的shell命令；换成自己想要执行的命令
    # 自己使用ssh时，命令怎么敲的command参数就怎么写
    command = 'ps -ef'
    # 实例化
    my_ssh_client = MySshClient()
    # 登录，如果返回结果为1000，那么执行命令，然后退出
    if my_ssh_client.ssh_login_rsa(host_ip,username,rsafile) == 1000:
        logging.warning(f"{host_ip}-login success, will execute command：{command}")
        my_ssh_client.execute_some_command(command)
        my_ssh_client.ssh_logout()
